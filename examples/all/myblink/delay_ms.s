.section .text
.globl delay_ms
delay_ms:
	
	MOVW R2, #42000
	MUL R0, R0, R2 

loop:
	CMP R1, R0
	BGE exit_loop
	ADD R1, R1, #1
	B loop

exit_loop:
	BX LR 
