#include "stm32f4xx.h"
#include "delay.h"
#include "usart.h"

int main(void)
{
	uint32_t utmp32 = 0;
	initUSART2(USART_9600);
	
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN; 								
	GPIOD->MODER |= GPIO_MODER_MODER12_0;  								
	GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR12_1;						
	
	while(1)
	{
		printUSART2("-> CNT: [%d]\n", utmp32);
		utmp32++;
		GPIOD->ODR ^= (1<<12);
		delay_ms(250);
	}
}
