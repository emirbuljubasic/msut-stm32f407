#include "stm32f4xx.h"
#include "lvgl/lvgl.h"
#include "delay.h"

#define CS  "PA4"	//  (SPI_NSS)
#define SCL	"PA5"	//  (SPI_SCK)
#define RST	"PA6"	//  (set to LOW for a brief moment then permamently to HIGH)
#define SDA	"PA7"	//  (SPI_MOSI)
#define DC	"PB5"	//  (SPI_MOSI)
					

#define D0 "PE8"
#define D1 "PE9"
#define D2 "PE10"
#define D3 "PE11"
#define D4 "PE12"   // all fsmc?
#define D5 "PE13"
#define D6 "PE14"
#define D7 "PE15"

#define RD  "PC6"
#define WR  "PC7"
#define RS  "PC8"	// all gpio
#define CS  "PC9"
#define RST "PC10"

void initializeGPIO() {

	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN; // Enable the clock of port A of the GPIO
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN; // Enable the clock of port B of the GPIO
					
	/* PORT C */
	/* 0000 0000 0001 0101 0101 0000 0000 0000 */
	/*    0    0    1    5    5    0    0    0 */
	/* PORT E */
	/* 0000 0000 0000 0000 0000 0100 0000 0000 */
	/*    0	   0	0	 0	  0	   8	0	 0 */
	// SET IO pin function modes
	GPIOC->MODER |= 0x00155000; 
	/* GPIOE->MODER |= 0x00000000; */

	/* PORT C                        7654 3210 */     	
	/* 0000 0000 0000 0000 0000 0000 0000 0000 */
	/* PORT E */
	/* 0000 0000 0000 0000 0000 0000 0000 0000 */

	// SET OUTPUT TYPE (PUSH-PULL)
	GPIOC->OTYPER |= 0x00000000;
	GPIOE->OTYPER |= 0x00000000;

	// SET HIGH-LOW transmission speed (50 MHz)
	GPIOC->OSPEEDR |= 0x002AA000;
	/* GPIOE->OSPEEDR |= 0x00000800; */

	// SET RESET PIN TO HIGH
	GPIOC->BSRRH = 0x0400; // set RST pin to LOW
	GPIOC->BSRRL = 0x0200; // set CS pin to HIGH
	delay_ms(5);
	GPIOC->BSRRL = 0x0440; // set RST pin to HIGH, set RD pin to HIGH
	GPIOC->BSRRH = 0x0200; // set CS pin to LOW


	// SET ALTERNATE PINS
	// 0101 0000 0101 0101 0000 0000 0000 0000
	/* GPIOE->AFR[0] = 0x50550000; */

}

// pseudo code
/* void writeCommand(const uint32_t* cmd, const uint32_t* params) { */
/* 	RD.setHigh(); */
/* 	DC.setLow(); */
/* 	WR.setLow(); */

/* 	delay_ms(2); */
/* 	WR.setHigh(); */
/* 	sendDBData(cmd); */

/* 	DC.setHIGH(); */
/* 	WR.setLow(); */

/* 	delay_ms(2); */
/* 	WR.setHigh(); */
/* 	sendDBData(params); */
/* } */

int main(void)
{

	initializeGPIO();

	while(1) {
		/* GPIOC->BSRRH = 0x0400; */
		delay_ms(2000);
		/* GPIOC->BSRRL = 0x0400; */
	}

/* 	Write command mode:				  RD -> H / DC -> L / WR -> RISING EDGE */
/* 	Write parameeter of display data: RD -> H / DC -> H / WR -> RISING EDGE */
/* 	RD -> PC5 */
/* 	DC -> PC9 */
/* 	WR -> PC6 */

	//probaj samo resetovati ga koristeci RST pin
	return 0;
}
