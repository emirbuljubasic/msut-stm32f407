	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 2
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"main.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.align	2
	.global	initializeGPIO
	.thumb
	.thumb_func
	.type	initializeGPIO, %function
initializeGPIO:
.LFB110:
	.file 1 "main.c"
	.loc 1 10 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 13 0
	ldr	r0, .L9
	.loc 1 23 0
	ldr	r3, .L9+4
	.loc 1 24 0
	ldr	r1, .L9+8
	.loc 1 41 0
	ldr	r2, .L9+12
	.loc 1 10 0
	push	{r4}
	.cfi_def_cfa_offset 4
	.cfi_offset 4, -4
	.loc 1 13 0
	ldr	r4, [r0, #48]
	orr	r4, r4, #1
	str	r4, [r0, #48]
	.loc 1 14 0
	ldr	r4, [r0, #48]
	orr	r4, r4, #2
	str	r4, [r0, #48]
	.loc 1 23 0
	ldr	r0, [r3]
	orr	r0, r0, #39424
	str	r0, [r3]
	.loc 1 24 0
	ldr	r0, [r1]
	orr	r0, r0, #1024
	str	r0, [r1]
	.loc 1 32 0
	ldr	r0, [r3, #4]
	str	r0, [r3, #4]
	.loc 1 33 0
	ldr	r0, [r1, #4]
	str	r0, [r1, #4]
	.loc 1 36 0
	ldr	r0, [r3, #8]
	orr	r0, r0, #43520
	str	r0, [r3, #8]
	.loc 1 37 0
	ldr	r0, [r1, #8]
	.loc 1 10 0
	sub	sp, sp, #12
	.cfi_def_cfa_offset 16
	.loc 1 40 0
	movs	r4, #0
	.loc 1 37 0
	orr	r0, r0, #2048
	str	r0, [r1, #8]
	.loc 1 40 0
	strh	r4, [r3, #24]	@ movhi
	.loc 1 41 0
	str	r4, [sp, #4]
	ldr	r3, [sp, #4]
	cmp	r3, r2
	bhi	.L3
.L4:
	.loc 1 41 0 is_stmt 0 discriminator 3
	ldr	r3, [sp, #4]
	adds	r3, r3, #1
	str	r3, [sp, #4]
	ldr	r3, [sp, #4]
	cmp	r3, r2
	bls	.L4
.L3:
	.loc 1 42 0 is_stmt 1
	ldr	r3, .L9+4
	.loc 1 47 0
	ldr	r2, .L9+8
	.loc 1 46 0
	ldr	r0, .L9+16
	.loc 1 42 0
	movs	r4, #64
	.loc 1 47 0
	mov	r1, #5242880
	.loc 1 42 0
	strh	r4, [r3, #24]	@ movhi
	.loc 1 46 0
	str	r0, [r3, #32]
	.loc 1 47 0
	str	r1, [r2, #32]
	.loc 1 49 0
	add	sp, sp, #12
	.cfi_def_cfa_offset 4
	@ sp needed
	ldr	r4, [sp], #4
	.cfi_restore 4
	.cfi_def_cfa_offset 0
	bx	lr
.L10:
	.align	2
.L9:
	.word	1073887232
	.word	1073872896
	.word	1073873920
	.word	99999
	.word	1347747840
	.cfi_endproc
.LFE110:
	.size	initializeGPIO, .-initializeGPIO
	.align	2
	.global	initializeSPI
	.thumb
	.thumb_func
	.type	initializeSPI, %function
initializeSPI:
.LFB111:
	.loc 1 51 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 57 0
	ldr	r3, .L12
	movw	r2, #2118
	strh	r2, [r3]	@ movhi
	bx	lr
.L13:
	.align	2
.L12:
	.word	1073819648
	.cfi_endproc
.LFE111:
	.size	initializeSPI, .-initializeSPI
	.align	2
	.global	setDCHighLow
	.thumb
	.thumb_func
	.type	setDCHighLow, %function
setDCHighLow:
.LFB112:
	.loc 1 60 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LVL0:
	.loc 1 63 0
	ldr	r3, .L18
	.loc 1 62 0
	cbnz	r0, .L17
	.loc 1 65 0
	mov	r2, #8192
	strh	r2, [r3, #24]	@ movhi
	bx	lr
.L17:
	.loc 1 63 0
	movs	r2, #32
	strh	r2, [r3, #24]	@ movhi
	bx	lr
.L19:
	.align	2
.L18:
	.word	1073873920
	.cfi_endproc
.LFE112:
	.size	setDCHighLow, .-setDCHighLow
	.align	2
	.global	txByteSPI1
	.thumb
	.thumb_func
	.type	txByteSPI1, %function
txByteSPI1:
.LFB113:
	.loc 1 70 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LVL1:
	.loc 1 73 0
	ldr	r3, .L21
	strh	r0, [r3, #12]	@ movhi
	.loc 1 78 0
	ldrh	r0, [r3, #12]
.LVL2:
	.loc 1 83 0
	uxth	r0, r0
	bx	lr
.L22:
	.align	2
.L21:
	.word	1073819648
	.cfi_endproc
.LFE113:
	.size	txByteSPI1, .-txByteSPI1
	.section	.text.startup,"ax",%progbits
	.align	2
	.global	main
	.thumb
	.thumb_func
	.type	main, %function
main:
.LFB114:
	.loc 1 86 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	push	{r3, r4, r5, r6, r7, lr}
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	.cfi_offset 4, -20
	.cfi_offset 5, -16
	.cfi_offset 6, -12
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	.loc 1 88 0
	bl	initializeGPIO
.LVL3:
.LBB24:
.LBB25:
	.loc 1 57 0
	ldr	r3, .L27
.LBE25:
.LBE24:
.LBB28:
.LBB29:
	.loc 1 65 0
	ldr	r2, .L27+4
.LBE29:
.LBE28:
.LBB32:
.LBB26:
	.loc 1 57 0
	movw	r0, #2118
.LBE26:
.LBE32:
.LBB33:
.LBB34:
	.loc 1 73 0
	movs	r1, #42
.LBE34:
.LBE33:
.LBB36:
.LBB30:
	.loc 1 65 0
	mov	r4, #8192
.LBE30:
.LBE36:
.LBB37:
.LBB27:
	.loc 1 57 0
	strh	r0, [r3]	@ movhi
.LVL4:
.LBE27:
.LBE37:
.LBB38:
.LBB31:
	.loc 1 65 0
	strh	r4, [r2, #24]	@ movhi
.LVL5:
.LBE31:
.LBE38:
.LBB39:
.LBB40:
	.loc 1 63 0
	movs	r0, #32
.LBE40:
.LBE39:
.LBB42:
.LBB35:
	.loc 1 73 0
	strh	r1, [r3, #12]	@ movhi
	.loc 1 78 0
	ldrh	r1, [r3, #12]
.LVL6:
.LBE35:
.LBE42:
.LBB43:
.LBB41:
	.loc 1 63 0
	strh	r0, [r2, #24]	@ movhi
.LVL7:
	movw	r1, #50000
.LBE41:
.LBE43:
.LBB44:
.LBB45:
	.loc 1 73 0
	mov	r7, #43	@ movhi
.LBE45:
.LBE44:
.LBB47:
.LBB48:
	movs	r6, #44
.LBE48:
.LBE47:
.LBB50:
.LBB51:
	movw	r5, #4095
.LVL8:
.L24:
.LBE51:
.LBE50:
.LBB53:
.LBB54:
	.loc 1 65 0
	strh	r4, [r2, #24]	@ movhi
.LVL9:
.LBE54:
.LBE53:
.LBB55:
.LBB46:
	.loc 1 73 0
	strh	r7, [r3, #12]	@ movhi
	.loc 1 78 0
	ldrh	lr, [r3, #12]
.LVL10:
.LBE46:
.LBE55:
.LBB56:
.LBB57:
	.loc 1 63 0
	strh	r0, [r2, #24]	@ movhi
.LVL11:
.LBE57:
.LBE56:
.LBB58:
.LBB59:
	.loc 1 65 0
	strh	r4, [r2, #24]	@ movhi
.LVL12:
.LBE59:
.LBE58:
.LBB60:
.LBB49:
	.loc 1 73 0
	strh	r6, [r3, #12]	@ movhi
	.loc 1 78 0
	ldrh	lr, [r3, #12]
.LVL13:
.LBE49:
.LBE60:
.LBB61:
.LBB62:
	.loc 1 63 0
	strh	r0, [r2, #24]	@ movhi
.LVL14:
.LBE62:
.LBE61:
	.loc 1 101 0
	subs	r1, r1, #1
.LVL15:
.LBB63:
.LBB52:
	.loc 1 73 0
	strh	r5, [r3, #12]	@ movhi
	.loc 1 78 0
	ldrh	lr, [r3, #12]
.LVL16:
.LBE52:
.LBE63:
	.loc 1 101 0
	bne	.L24
	.loc 1 117 0
	pop	{r3, r4, r5, r6, r7, pc}
.L28:
	.align	2
.L27:
	.word	1073819648
	.word	1073873920
	.cfi_endproc
.LFE114:
	.size	main, .-main
	.text
.Letext0:
	.file 2 "/home/emir/msut/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/machine/_default_types.h"
	.file 3 "/home/emir/msut/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdint.h"
	.file 4 "stm32f4xx.h"
	.file 5 "../../../STM32F407/Libraries/CMSIS/Include/core_cm4.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x648
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF61
	.byte	0x1
	.4byte	.LASF62
	.4byte	.LASF63
	.4byte	.Ldebug_ranges0+0xb8
	.4byte	0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x2b
	.4byte	0x45
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x3f
	.4byte	0x57
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x41
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF10
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x3
	.4byte	.LASF12
	.byte	0x3
	.byte	0x21
	.4byte	0x3a
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x3
	.byte	0x2c
	.4byte	0x4c
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x2d
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF15
	.uleb128 0x5
	.4byte	0xa2
	.uleb128 0x6
	.4byte	0xa2
	.4byte	0xc9
	.uleb128 0x7
	.4byte	0xad
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0x8c
	.uleb128 0x5
	.4byte	0x97
	.uleb128 0x8
	.byte	0x28
	.byte	0x4
	.2byte	0x28e
	.4byte	0x15f
	.uleb128 0x9
	.4byte	.LASF16
	.byte	0x4
	.2byte	0x290
	.4byte	0xb4
	.byte	0
	.uleb128 0x9
	.4byte	.LASF17
	.byte	0x4
	.2byte	0x291
	.4byte	0xb4
	.byte	0x4
	.uleb128 0x9
	.4byte	.LASF18
	.byte	0x4
	.2byte	0x292
	.4byte	0xb4
	.byte	0x8
	.uleb128 0x9
	.4byte	.LASF19
	.byte	0x4
	.2byte	0x293
	.4byte	0xb4
	.byte	0xc
	.uleb128 0xa
	.ascii	"IDR\000"
	.byte	0x4
	.2byte	0x294
	.4byte	0xb4
	.byte	0x10
	.uleb128 0xa
	.ascii	"ODR\000"
	.byte	0x4
	.2byte	0x295
	.4byte	0xb4
	.byte	0x14
	.uleb128 0x9
	.4byte	.LASF20
	.byte	0x4
	.2byte	0x296
	.4byte	0xc9
	.byte	0x18
	.uleb128 0x9
	.4byte	.LASF21
	.byte	0x4
	.2byte	0x297
	.4byte	0xc9
	.byte	0x1a
	.uleb128 0x9
	.4byte	.LASF22
	.byte	0x4
	.2byte	0x298
	.4byte	0xb4
	.byte	0x1c
	.uleb128 0xa
	.ascii	"AFR\000"
	.byte	0x4
	.2byte	0x299
	.4byte	0x15f
	.byte	0x20
	.byte	0
	.uleb128 0x5
	.4byte	0xb9
	.uleb128 0xb
	.4byte	.LASF23
	.byte	0x4
	.2byte	0x29a
	.4byte	0xd3
	.uleb128 0x8
	.byte	0x88
	.byte	0x4
	.2byte	0x2dd
	.4byte	0x2ff
	.uleb128 0xa
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x2df
	.4byte	0xb4
	.byte	0
	.uleb128 0x9
	.4byte	.LASF24
	.byte	0x4
	.2byte	0x2e0
	.4byte	0xb4
	.byte	0x4
	.uleb128 0x9
	.4byte	.LASF25
	.byte	0x4
	.2byte	0x2e1
	.4byte	0xb4
	.byte	0x8
	.uleb128 0xa
	.ascii	"CIR\000"
	.byte	0x4
	.2byte	0x2e2
	.4byte	0xb4
	.byte	0xc
	.uleb128 0x9
	.4byte	.LASF26
	.byte	0x4
	.2byte	0x2e3
	.4byte	0xb4
	.byte	0x10
	.uleb128 0x9
	.4byte	.LASF27
	.byte	0x4
	.2byte	0x2e4
	.4byte	0xb4
	.byte	0x14
	.uleb128 0x9
	.4byte	.LASF28
	.byte	0x4
	.2byte	0x2e5
	.4byte	0xb4
	.byte	0x18
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x2e6
	.4byte	0xa2
	.byte	0x1c
	.uleb128 0x9
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x2e7
	.4byte	0xb4
	.byte	0x20
	.uleb128 0x9
	.4byte	.LASF31
	.byte	0x4
	.2byte	0x2e8
	.4byte	0xb4
	.byte	0x24
	.uleb128 0x9
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x2e9
	.4byte	0xb9
	.byte	0x28
	.uleb128 0x9
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x2ea
	.4byte	0xb4
	.byte	0x30
	.uleb128 0x9
	.4byte	.LASF34
	.byte	0x4
	.2byte	0x2eb
	.4byte	0xb4
	.byte	0x34
	.uleb128 0x9
	.4byte	.LASF35
	.byte	0x4
	.2byte	0x2ec
	.4byte	0xb4
	.byte	0x38
	.uleb128 0x9
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x2ed
	.4byte	0xa2
	.byte	0x3c
	.uleb128 0x9
	.4byte	.LASF37
	.byte	0x4
	.2byte	0x2ee
	.4byte	0xb4
	.byte	0x40
	.uleb128 0x9
	.4byte	.LASF38
	.byte	0x4
	.2byte	0x2ef
	.4byte	0xb4
	.byte	0x44
	.uleb128 0x9
	.4byte	.LASF39
	.byte	0x4
	.2byte	0x2f0
	.4byte	0xb9
	.byte	0x48
	.uleb128 0x9
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x2f1
	.4byte	0xb4
	.byte	0x50
	.uleb128 0x9
	.4byte	.LASF41
	.byte	0x4
	.2byte	0x2f2
	.4byte	0xb4
	.byte	0x54
	.uleb128 0x9
	.4byte	.LASF42
	.byte	0x4
	.2byte	0x2f3
	.4byte	0xb4
	.byte	0x58
	.uleb128 0x9
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x2f4
	.4byte	0xa2
	.byte	0x5c
	.uleb128 0x9
	.4byte	.LASF44
	.byte	0x4
	.2byte	0x2f5
	.4byte	0xb4
	.byte	0x60
	.uleb128 0x9
	.4byte	.LASF45
	.byte	0x4
	.2byte	0x2f6
	.4byte	0xb4
	.byte	0x64
	.uleb128 0x9
	.4byte	.LASF46
	.byte	0x4
	.2byte	0x2f7
	.4byte	0xb9
	.byte	0x68
	.uleb128 0x9
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x2f8
	.4byte	0xb4
	.byte	0x70
	.uleb128 0xa
	.ascii	"CSR\000"
	.byte	0x4
	.2byte	0x2f9
	.4byte	0xb4
	.byte	0x74
	.uleb128 0x9
	.4byte	.LASF48
	.byte	0x4
	.2byte	0x2fa
	.4byte	0xb9
	.byte	0x78
	.uleb128 0x9
	.4byte	.LASF49
	.byte	0x4
	.2byte	0x2fb
	.4byte	0xb4
	.byte	0x80
	.uleb128 0x9
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x2fc
	.4byte	0xb4
	.byte	0x84
	.byte	0
	.uleb128 0xb
	.4byte	.LASF51
	.byte	0x4
	.2byte	0x2fd
	.4byte	0x170
	.uleb128 0x8
	.byte	0x24
	.byte	0x4
	.2byte	0x34f
	.4byte	0x3fd
	.uleb128 0xa
	.ascii	"CR1\000"
	.byte	0x4
	.2byte	0x351
	.4byte	0xc9
	.byte	0
	.uleb128 0x9
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x352
	.4byte	0x8c
	.byte	0x2
	.uleb128 0xa
	.ascii	"CR2\000"
	.byte	0x4
	.2byte	0x353
	.4byte	0xc9
	.byte	0x4
	.uleb128 0x9
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x354
	.4byte	0x8c
	.byte	0x6
	.uleb128 0xa
	.ascii	"SR\000"
	.byte	0x4
	.2byte	0x355
	.4byte	0xc9
	.byte	0x8
	.uleb128 0x9
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x356
	.4byte	0x8c
	.byte	0xa
	.uleb128 0xa
	.ascii	"DR\000"
	.byte	0x4
	.2byte	0x357
	.4byte	0xc9
	.byte	0xc
	.uleb128 0x9
	.4byte	.LASF39
	.byte	0x4
	.2byte	0x358
	.4byte	0x8c
	.byte	0xe
	.uleb128 0x9
	.4byte	.LASF52
	.byte	0x4
	.2byte	0x359
	.4byte	0xc9
	.byte	0x10
	.uleb128 0x9
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x35a
	.4byte	0x8c
	.byte	0x12
	.uleb128 0x9
	.4byte	.LASF53
	.byte	0x4
	.2byte	0x35b
	.4byte	0xc9
	.byte	0x14
	.uleb128 0x9
	.4byte	.LASF46
	.byte	0x4
	.2byte	0x35c
	.4byte	0x8c
	.byte	0x16
	.uleb128 0x9
	.4byte	.LASF54
	.byte	0x4
	.2byte	0x35d
	.4byte	0xc9
	.byte	0x18
	.uleb128 0x9
	.4byte	.LASF48
	.byte	0x4
	.2byte	0x35e
	.4byte	0x8c
	.byte	0x1a
	.uleb128 0x9
	.4byte	.LASF55
	.byte	0x4
	.2byte	0x35f
	.4byte	0xc9
	.byte	0x1c
	.uleb128 0x9
	.4byte	.LASF56
	.byte	0x4
	.2byte	0x360
	.4byte	0x8c
	.byte	0x1e
	.uleb128 0x9
	.4byte	.LASF57
	.byte	0x4
	.2byte	0x361
	.4byte	0xc9
	.byte	0x20
	.uleb128 0x9
	.4byte	.LASF58
	.byte	0x4
	.2byte	0x362
	.4byte	0x8c
	.byte	0x22
	.byte	0
	.uleb128 0xb
	.4byte	.LASF59
	.byte	0x4
	.2byte	0x363
	.4byte	0x30b
	.uleb128 0xc
	.4byte	.LASF64
	.byte	0x1
	.byte	0x33
	.byte	0x1
	.uleb128 0xd
	.4byte	.LASF65
	.byte	0x1
	.byte	0x3c
	.byte	0x1
	.4byte	0x429
	.uleb128 0xe
	.ascii	"opt\000"
	.byte	0x1
	.byte	0x3c
	.4byte	0x7e
	.byte	0
	.uleb128 0xf
	.4byte	.LASF66
	.byte	0x1
	.byte	0x45
	.4byte	0x8c
	.byte	0x1
	.4byte	0x450
	.uleb128 0x10
	.4byte	.LASF60
	.byte	0x1
	.byte	0x45
	.4byte	0x8c
	.uleb128 0x11
	.ascii	"tmp\000"
	.byte	0x1
	.byte	0x47
	.4byte	0x8c
	.byte	0
	.uleb128 0x12
	.4byte	.LASF67
	.byte	0x1
	.byte	0xa
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x472
	.uleb128 0x13
	.ascii	"i\000"
	.byte	0x1
	.byte	0xb
	.4byte	0xb4
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x14
	.4byte	0x409
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x15
	.4byte	0x411
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x49c
	.uleb128 0x16
	.4byte	0x41d
	.uleb128 0x1
	.byte	0x50
	.byte	0
	.uleb128 0x15
	.4byte	0x429
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x4c0
	.uleb128 0x17
	.4byte	0x439
	.4byte	.LLST0
	.uleb128 0x18
	.4byte	0x444
	.uleb128 0x1
	.byte	0x50
	.byte	0
	.uleb128 0x19
	.4byte	.LASF68
	.byte	0x1
	.byte	0x55
	.4byte	0x7e
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x63f
	.uleb128 0x1a
	.ascii	"i\000"
	.byte	0x1
	.byte	0x57
	.4byte	0xa2
	.4byte	.LLST1
	.uleb128 0x1b
	.4byte	.LASF60
	.byte	0x1
	.byte	0x5c
	.4byte	0x8c
	.4byte	.LLST2
	.uleb128 0x1c
	.4byte	0x409
	.4byte	.LBB24
	.4byte	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0x5a
	.uleb128 0x1d
	.4byte	0x411
	.4byte	.LBB28
	.4byte	.Ldebug_ranges0+0x20
	.byte	0x1
	.byte	0x5e
	.4byte	0x51e
	.uleb128 0x1e
	.4byte	0x41d
	.byte	0
	.byte	0
	.uleb128 0x1d
	.4byte	0x429
	.4byte	.LBB33
	.4byte	.Ldebug_ranges0+0x40
	.byte	0x1
	.byte	0x60
	.4byte	0x543
	.uleb128 0x1e
	.4byte	0x439
	.byte	0x2a
	.uleb128 0x1f
	.4byte	.Ldebug_ranges0+0x40
	.uleb128 0x20
	.4byte	0x444
	.byte	0
	.byte	0
	.uleb128 0x1d
	.4byte	0x411
	.4byte	.LBB39
	.4byte	.Ldebug_ranges0+0x58
	.byte	0x1
	.byte	0x61
	.4byte	0x55d
	.uleb128 0x1e
	.4byte	0x41d
	.byte	0x1
	.byte	0
	.uleb128 0x1d
	.4byte	0x429
	.4byte	.LBB44
	.4byte	.Ldebug_ranges0+0x70
	.byte	0x1
	.byte	0x68
	.4byte	0x582
	.uleb128 0x1e
	.4byte	0x439
	.byte	0x2b
	.uleb128 0x1f
	.4byte	.Ldebug_ranges0+0x70
	.uleb128 0x20
	.4byte	0x444
	.byte	0
	.byte	0
	.uleb128 0x1d
	.4byte	0x429
	.4byte	.LBB47
	.4byte	.Ldebug_ranges0+0x88
	.byte	0x1
	.byte	0x6c
	.4byte	0x5a7
	.uleb128 0x1e
	.4byte	0x439
	.byte	0x2c
	.uleb128 0x1f
	.4byte	.Ldebug_ranges0+0x88
	.uleb128 0x20
	.4byte	0x444
	.byte	0
	.byte	0
	.uleb128 0x1d
	.4byte	0x429
	.4byte	.LBB50
	.4byte	.Ldebug_ranges0+0xa0
	.byte	0x1
	.byte	0x71
	.4byte	0x5cd
	.uleb128 0x21
	.4byte	0x439
	.2byte	0xfff
	.uleb128 0x1f
	.4byte	.Ldebug_ranges0+0xa0
	.uleb128 0x20
	.4byte	0x444
	.byte	0
	.byte	0
	.uleb128 0x22
	.4byte	0x411
	.4byte	.LBB53
	.4byte	.LBE53-.LBB53
	.byte	0x1
	.byte	0x66
	.4byte	0x5e7
	.uleb128 0x1e
	.4byte	0x41d
	.byte	0
	.byte	0
	.uleb128 0x22
	.4byte	0x411
	.4byte	.LBB56
	.4byte	.LBE56-.LBB56
	.byte	0x1
	.byte	0x69
	.4byte	0x601
	.uleb128 0x1e
	.4byte	0x41d
	.byte	0x1
	.byte	0
	.uleb128 0x22
	.4byte	0x411
	.4byte	.LBB58
	.4byte	.LBE58-.LBB58
	.byte	0x1
	.byte	0x6a
	.4byte	0x61b
	.uleb128 0x1e
	.4byte	0x41d
	.byte	0
	.byte	0
	.uleb128 0x22
	.4byte	0x411
	.4byte	.LBB61
	.4byte	.LBE61-.LBB61
	.byte	0x1
	.byte	0x6e
	.4byte	0x635
	.uleb128 0x1e
	.4byte	0x41d
	.byte	0x1
	.byte	0
	.uleb128 0x23
	.4byte	.LVL3
	.4byte	0x450
	.byte	0
	.uleb128 0x24
	.4byte	.LASF69
	.byte	0x5
	.2byte	0x51b
	.4byte	0xce
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x1d
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x55
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LVL1
	.4byte	.LVL2
	.2byte	0x1
	.byte	0x50
	.4byte	.LVL2
	.4byte	.LFE113
	.2byte	0x2
	.byte	0x73
	.sleb128 12
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LVL7
	.4byte	.LVL8
	.2byte	0x2
	.byte	0x30
	.byte	0x9f
	.4byte	.LVL8
	.4byte	.LVL15
	.2byte	0x7
	.byte	0xa
	.2byte	0xc350
	.byte	0x71
	.sleb128 0
	.byte	0x1c
	.byte	0x9f
	.4byte	.LVL15
	.4byte	.LVL16
	.2byte	0x7
	.byte	0xa
	.2byte	0xc34f
	.byte	0x71
	.sleb128 0
	.byte	0x1c
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST2:
	.4byte	.LVL4
	.4byte	.LVL5
	.2byte	0x4
	.byte	0xa
	.2byte	0xfff
	.byte	0x9f
	.4byte	.LVL5
	.4byte	.LVL8
	.2byte	0x3
	.byte	0x8
	.byte	0x2a
	.byte	0x9f
	.4byte	.LVL9
	.4byte	.LVL12
	.2byte	0x3
	.byte	0x8
	.byte	0x2b
	.byte	0x9f
	.4byte	.LVL12
	.4byte	.LVL14
	.2byte	0x3
	.byte	0x8
	.byte	0x2c
	.byte	0x9f
	.4byte	.LVL14
	.4byte	.LFE114
	.2byte	0x4
	.byte	0xa
	.2byte	0xfff
	.byte	0x9f
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x24
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",%progbits
.Ldebug_ranges0:
	.4byte	.LBB24
	.4byte	.LBE24
	.4byte	.LBB32
	.4byte	.LBE32
	.4byte	.LBB37
	.4byte	.LBE37
	.4byte	0
	.4byte	0
	.4byte	.LBB28
	.4byte	.LBE28
	.4byte	.LBB36
	.4byte	.LBE36
	.4byte	.LBB38
	.4byte	.LBE38
	.4byte	0
	.4byte	0
	.4byte	.LBB33
	.4byte	.LBE33
	.4byte	.LBB42
	.4byte	.LBE42
	.4byte	0
	.4byte	0
	.4byte	.LBB39
	.4byte	.LBE39
	.4byte	.LBB43
	.4byte	.LBE43
	.4byte	0
	.4byte	0
	.4byte	.LBB44
	.4byte	.LBE44
	.4byte	.LBB55
	.4byte	.LBE55
	.4byte	0
	.4byte	0
	.4byte	.LBB47
	.4byte	.LBE47
	.4byte	.LBB60
	.4byte	.LBE60
	.4byte	0
	.4byte	0
	.4byte	.LBB50
	.4byte	.LBE50
	.4byte	.LBB63
	.4byte	.LBE63
	.4byte	0
	.4byte	0
	.4byte	.Ltext0
	.4byte	.Letext0
	.4byte	.LFB114
	.4byte	.LFE114
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF40:
	.ascii	"AHB1LPENR\000"
.LASF18:
	.ascii	"OSPEEDR\000"
.LASF30:
	.ascii	"APB1RSTR\000"
.LASF34:
	.ascii	"AHB2ENR\000"
.LASF2:
	.ascii	"short int\000"
.LASF15:
	.ascii	"sizetype\000"
.LASF47:
	.ascii	"BDCR\000"
.LASF50:
	.ascii	"PLLI2SCFGR\000"
.LASF68:
	.ascii	"main\000"
.LASF7:
	.ascii	"__uint32_t\000"
.LASF4:
	.ascii	"__uint16_t\000"
.LASF49:
	.ascii	"SSCGR\000"
.LASF54:
	.ascii	"TXCRCR\000"
.LASF42:
	.ascii	"AHB3LPENR\000"
.LASF53:
	.ascii	"RXCRCR\000"
.LASF39:
	.ascii	"RESERVED3\000"
.LASF43:
	.ascii	"RESERVED4\000"
.LASF65:
	.ascii	"setDCHighLow\000"
.LASF37:
	.ascii	"APB1ENR\000"
.LASF17:
	.ascii	"OTYPER\000"
.LASF35:
	.ascii	"AHB3ENR\000"
.LASF9:
	.ascii	"long long int\000"
.LASF19:
	.ascii	"PUPDR\000"
.LASF6:
	.ascii	"long int\000"
.LASF51:
	.ascii	"RCC_TypeDef\000"
.LASF21:
	.ascii	"BSRRH\000"
.LASF16:
	.ascii	"MODER\000"
.LASF31:
	.ascii	"APB2RSTR\000"
.LASF20:
	.ascii	"BSRRL\000"
.LASF63:
	.ascii	"/home/emir/msut/STM32F407/examples/myblink\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF38:
	.ascii	"APB2ENR\000"
.LASF66:
	.ascii	"txByteSPI1\000"
.LASF0:
	.ascii	"signed char\000"
.LASF10:
	.ascii	"long long unsigned int\000"
.LASF14:
	.ascii	"uint32_t\000"
.LASF55:
	.ascii	"I2SCFGR\000"
.LASF11:
	.ascii	"unsigned int\000"
.LASF27:
	.ascii	"AHB2RSTR\000"
.LASF12:
	.ascii	"uint16_t\000"
.LASF8:
	.ascii	"long unsigned int\000"
.LASF25:
	.ascii	"CFGR\000"
.LASF24:
	.ascii	"PLLCFGR\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF44:
	.ascii	"APB1LPENR\000"
.LASF13:
	.ascii	"int32_t\000"
.LASF59:
	.ascii	"SPI_TypeDef\000"
.LASF57:
	.ascii	"I2SPR\000"
.LASF60:
	.ascii	"data\000"
.LASF41:
	.ascii	"AHB2LPENR\000"
.LASF52:
	.ascii	"CRCPR\000"
.LASF29:
	.ascii	"RESERVED0\000"
.LASF32:
	.ascii	"RESERVED1\000"
.LASF36:
	.ascii	"RESERVED2\000"
.LASF61:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O2 -fsingle-precision-constant\000"
.LASF62:
	.ascii	"main.c\000"
.LASF46:
	.ascii	"RESERVED5\000"
.LASF48:
	.ascii	"RESERVED6\000"
.LASF56:
	.ascii	"RESERVED7\000"
.LASF58:
	.ascii	"RESERVED8\000"
.LASF33:
	.ascii	"AHB1ENR\000"
.LASF67:
	.ascii	"initializeGPIO\000"
.LASF5:
	.ascii	"__int32_t\000"
.LASF64:
	.ascii	"initializeSPI\000"
.LASF26:
	.ascii	"AHB1RSTR\000"
.LASF23:
	.ascii	"GPIO_TypeDef\000"
.LASF28:
	.ascii	"AHB3RSTR\000"
.LASF69:
	.ascii	"ITM_RxBuffer\000"
.LASF22:
	.ascii	"LCKR\000"
.LASF45:
	.ascii	"APB2LPENR\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
