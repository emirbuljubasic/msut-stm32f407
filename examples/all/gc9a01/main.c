#include "stm32f4xx.h"
#include "delay.h"

#define CS  "PA4"	//  (SPI_NSS)
#define SCL	"PA5"	//  (SPI_SCK)
#define RST	"PA6"	//  (set to LOW for a brief moment then permamently to HIGH)
#define SDA	"PA7"	//  (SPI_MOSI)
#define DC	"PB5"	//  (SPI_MOSI)

void initializeGPIO() {
	volatile uint32_t i;

	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN; // Enable the clock of port A of the GPIO
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN; // Enable the clock of port B of the GPIO
					
	/* PORT A */
	/* 0000 0000 0000 0000 1001 1010 0000 0000 */
	/*    0    0    0    0    9    A    0    0 */
	/* PORT B */
	/* 0000 0000 0000 0000 0000 0100 0000 0000 */
	/*    0	   0	0	 0	  0	   8	0	 0 */
	// SET IO pin function modes
	GPIOA->MODER |= 0x00009A00; 
	GPIOB->MODER |= 0x00000400;

	/* PORT A                        7654 3210 */     	
	/* 0000 0000 0000 0000 0000 0000 0000 0000 */
	/* PORT B */
	/* 0000 0000 0000 0000 0000 0000 0000 0000 */

	// SET OUTPUT TYPE (PUSH-PULL)
	GPIOA->OTYPER |= 0x00000000;
	GPIOB->OTYPER |= 0x00000000;

	// SET HIGH-LOW transmission speed (50 MHz)
	GPIOA->OSPEEDR |= 0x0000AA00;
	GPIOB->OSPEEDR |= 0x00000800;

	// SET RESET PIN TO HIGH
	GPIOA->BSRRL = 0x0000;
	for (i = 0; i < 100000; ++i) {}
	GPIOA->BSRRL = 0x0040;


	// SET ALTERNATE PINS
	GPIOA->AFR[0] = 0x50550000;
	GPIOB->AFR[0] = 0x00500000;

}

void initializeSPI() {
	// set TI mode Falling edge - idle LOW
	// set CPOL to 0 (or reset) for LOW idle
	// set CPHA to 1 (for falling edge)
	
	/* 0000 1000 0100 0110 */
	SPI1->CR1 = 0x0846;
}

void setDCHighLow(int opt) {
	//opt = 1 -> HIGH / opt = 0 -> LOW
	if (opt) {
		GPIOB->BSRRL = 0x0020;
	} else {
		GPIOB->BSRRL = 0x2000;	
	}
}

uint16_t txByteSPI1(uint16_t data)
{/// send data using SPI1
	uint16_t tmp;

	SPI1->DR = data; 													//
	/* while(!(SPI1->SR & SPI_I2S_FLAG_TXE));								// */ 
	/* while(!(SPI1->SR & SPI_I2S_FLAG_RXNE)); 							// */ 
	/* while(SPI1->SR & SPI_I2S_FLAG_BSY); 								// */ 
	/* if (SPI_SR_TXE == 1) { */	
		tmp = SPI1->DR; 													
	/* } */

		
	return tmp;
}

int main(void)
{
	uint32_t i;
	initializeGPIO();

	initializeSPI();

	uint16_t data = 0xFFF;

	setDCHighLow(0);
	data = 0x2a;
	txByteSPI1(data);
	setDCHighLow(1);

	//delay_ms(100);

	for (i=0; i < 50000; ++i) {
		setDCHighLow(0);
		data=0x2B;
		txByteSPI1(data);
		setDCHighLow(1);
		setDCHighLow(0);
		data = 0x2C;
		txByteSPI1(data);
	//	delay_ms(100);
		setDCHighLow(1);
		data=0xFFF;
	//	delay_ms(100);
		txByteSPI1(data);
	}

		
}






/* for (i = 0; i < 57600; ++i) { */
	/* 	txByteSPI1(data); */
	/* } */
	// to display only color red RGB(255,0,0), for a single pixel, loop this 240x240 times for full screen
	/* for (i = 0; i < 16; ++i) { */
	/* 	if (i < 5) { */
	/* 		//send BIT 1 */
	/* 	} else { */
	/* 		//send BIT 0 */
	/* 	} */	
	/* } */

