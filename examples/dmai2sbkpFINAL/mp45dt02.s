	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 2
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"mp45dt02.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.align	2
	.global	initMIC
	.thumb
	.thumb_func
	.type	initMIC, %function
initMIC:
.LFB110:
	.file 1 "mp45dt02.c"
	.loc 1 4 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LVL0:
	.loc 1 14 0
	ldr	r2, .L7
	.loc 1 18 0
	ldr	r1, .L7+4
	.loc 1 35 0
	ldr	r3, .L7+8
	.loc 1 4 0
	push	{r4, r5, r6}
	.cfi_def_cfa_offset 12
	.cfi_offset 4, -12
	.cfi_offset 5, -8
	.cfi_offset 6, -4
	.loc 1 14 0
	ldr	r5, [r2, #48]
	.loc 1 17 0
	ldr	r4, .L7+12
	.loc 1 36 0
	ldr	r6, .L7+16
	.loc 1 14 0
	orr	r5, r5, #2
	str	r5, [r2, #48]
	.loc 1 15 0
	ldr	r5, [r2, #48]
	orr	r5, r5, #4
	str	r5, [r2, #48]
	.loc 1 17 0
	ldr	r5, [r4]
	orr	r5, r5, #2097152
	str	r5, [r4]
	.loc 1 18 0
	ldr	r5, [r1]
	orr	r5, r5, #128
	str	r5, [r1]
	.loc 1 20 0
	ldr	r5, [r4, #36]
	orr	r5, r5, #1280
	str	r5, [r4, #36]
	.loc 1 21 0
	ldr	r5, [r1, #32]
	orr	r5, r5, #20480
	str	r5, [r1, #32]
	.loc 1 23 0
	ldr	r5, [r4, #8]
	orr	r5, r5, #2097152
	str	r5, [r4, #8]
	.loc 1 24 0
	ldr	r4, [r1, #8]
	orr	r4, r4, #128
	str	r4, [r1, #8]
	.loc 1 26 0
	ldr	r1, [r2, #64]
	orr	r1, r1, #16384
	str	r1, [r2, #64]
	.loc 1 27 0
	ldr	r1, [r2, #48]
	.loc 1 35 0
	lsls	r0, r0, #8
.LVL1:
	udiv	r0, r3, r0
	.loc 1 36 0
	adds	r0, r0, #5
	umull	r3, r0, r6, r0
	.loc 1 27 0
	orr	r1, r1, #4096
	.loc 1 36 0
	lsrs	r3, r0, #3
	.loc 1 27 0
	str	r1, [r2, #48]
.LVL2:
	.loc 1 39 0
	lsls	r1, r3, #31
	bmi	.L6
	.loc 1 47 0
	ldr	r3, .L7+20
.LVL3:
	ubfx	r0, r0, #4, #16
.LVL4:
	strh	r0, [r3, #32]	@ movhi
.LVL5:
.L3:
	.loc 1 50 0
	ldr	r3, .L7+20
	ldrh	r2, [r3, #32]
	uxth	r2, r2
	orr	r2, r2, #512
	strh	r2, [r3, #32]	@ movhi
	.loc 1 52 0
	ldrh	r2, [r3, #4]
	uxth	r2, r2
	orr	r2, r2, #1
	strh	r2, [r3, #4]	@ movhi
	.loc 1 53 0
	ldrh	r2, [r3, #4]
	uxth	r2, r2
	orr	r2, r2, #64
	.loc 1 56 0
	movw	r1, #2856
	.loc 1 53 0
	strh	r2, [r3, #4]	@ movhi
	.loc 1 62 0
	pop	{r4, r5, r6}
	.cfi_remember_state
	.cfi_restore 6
	.cfi_restore 5
	.cfi_restore 4
	.cfi_def_cfa_offset 0
	.loc 1 56 0
	strh	r1, [r3, #28]	@ movhi
	.loc 1 62 0
	bx	lr
.LVL6:
.L6:
	.cfi_restore_state
	.loc 1 41 0
	subs	r3, r3, #1
.LVL7:
	.loc 1 42 0
	lsrs	r3, r3, #1
.LVL8:
	.loc 1 43 0
	orr	r3, r3, #256
.LVL9:
	sub	r2, r2, #131072
	uxth	r3, r3
	strh	r3, [r2, #32]	@ movhi
	b	.L3
.L8:
	.align	2
.L7:
	.word	1073887232
	.word	1073874944
	.word	860000000
	.word	1073873920
	.word	-858993459
	.word	1073756160
	.cfi_endproc
.LFE110:
	.size	initMIC, .-initMIC
	.align	2
	.global	initDMA1
	.thumb
	.thumb_func
	.type	initDMA1, %function
initDMA1:
.LFB111:
	.loc 1 65 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LVL10:
	.loc 1 68 0
	ldr	r1, .L13
	.loc 1 70 0
	ldr	r2, .L13+4
	.loc 1 68 0
	ldr	r3, [r1, #48]
	orr	r3, r3, #2097152
	str	r3, [r1, #48]
	.loc 1 70 0
	ldr	r3, [r2]
	bic	r3, r3, #1
	str	r3, [r2]
.L10:
	.loc 1 71 0 discriminator 1
	ldr	r3, [r2]
	ldr	r1, .L13+4
	lsls	r3, r3, #31
	bmi	.L10
	.loc 1 65 0
	push	{r4, r5, r6, r7}
	.cfi_def_cfa_offset 16
	.cfi_offset 4, -16
	.cfi_offset 5, -12
	.cfi_offset 6, -8
	.cfi_offset 7, -4
	.loc 1 73 0
	ldr	r0, .L13+8
	ldr	r7, [r1]
	.loc 1 75 0
	ldr	r2, .L13+12
	.loc 1 76 0
	ldr	r6, .L13+16
	.loc 1 78 0
	ldr	r3, .L13+20
	ldr	r5, .L13+24
	.loc 1 79 0
	ldr	r4, .L13+28
	.loc 1 73 0
	ands	r0, r0, r7
	str	r0, [r1]
	.loc 1 75 0
	ldr	r1, [r2]
	ldr	r0, [r2]
	eors	r1, r1, r0
	str	r1, [r2]
	.loc 1 76 0
	ldr	r1, [r6, #4]
	ldr	r6, [r2, #4]
	.loc 1 81 0
	mov	r0, #512
	.loc 1 76 0
	eors	r1, r1, r6
	str	r1, [r2, #4]
	.loc 1 78 0
	str	r5, [r3, #8]
	.loc 1 79 0
	str	r4, [r3, #12]
	.loc 1 81 0
	str	r0, [r3, #4]
	.loc 1 84 0
	ldr	r2, [r3]
	bic	r2, r2, #234881024
	str	r2, [r3]
	.loc 1 85 0
	ldr	r2, [r3]
	orr	r2, r2, #131072
	str	r2, [r3]
	.loc 1 87 0
	ldr	r2, [r3]
	orr	r2, r2, #1024
	str	r2, [r3]
	.loc 1 89 0
	ldr	r2, [r3]
	orr	r2, r2, #2048
	str	r2, [r3]
	.loc 1 90 0
	ldr	r2, [r3]
	orr	r2, r2, #8192
	str	r2, [r3]
	.loc 1 91 0
	ldr	r2, [r3]
	orr	r2, r2, #16
	str	r2, [r3]
	.loc 1 93 0
	ldr	r2, [r3]
	bic	r2, r2, #192
	str	r2, [r3]
	.loc 1 95 0
	ldr	r2, [r3]
	orr	r2, r2, #1
	str	r2, [r3]
	.loc 1 112 0
	pop	{r4, r5, r6, r7}
	.cfi_restore 7
	.cfi_restore 6
	.cfi_restore 5
	.cfi_restore 4
	.cfi_def_cfa_offset 0
	bx	lr
.L14:
	.align	2
.L13:
	.word	1073887232
	.word	1073897584
	.word	-267386880
	.word	1073897472
	.word	1073898496
	.word	1073897560
	.word	1073756172
	.word	utmp16
	.cfi_endproc
.LFE111:
	.size	initDMA1, .-initDMA1
	.comm	utmp16,1024,4
	.comm	buffer,2048,4
.Letext0:
	.file 2 "/home/emir/msut/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/machine/_default_types.h"
	.file 3 "/home/emir/msut/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdint.h"
	.file 4 "stm32f4xx.h"
	.file 5 "../../../STM32F407/Libraries/CMSIS/Include/core_cm4.h"
	.file 6 "mp45dt02.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x58d
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF78
	.byte	0x1
	.4byte	.LASF79
	.4byte	.LASF80
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF0
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x2
	.byte	0x1d
	.4byte	0x45
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF3
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF4
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x2b
	.4byte	0x5e
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x3f
	.4byte	0x70
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF9
	.uleb128 0x3
	.4byte	.LASF10
	.byte	0x2
	.byte	0x41
	.4byte	0x82
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF12
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x3
	.byte	0x15
	.4byte	0x3a
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x21
	.4byte	0x53
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x2c
	.4byte	0x65
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x2d
	.4byte	0x77
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF17
	.uleb128 0x5
	.4byte	0xb8
	.uleb128 0x6
	.4byte	0xb8
	.4byte	0xdf
	.uleb128 0x7
	.4byte	0xc3
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0xa2
	.uleb128 0x5
	.4byte	0xad
	.uleb128 0x8
	.byte	0x18
	.byte	0x4
	.2byte	0x1d7
	.4byte	0x140
	.uleb128 0x9
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x1d9
	.4byte	0xca
	.byte	0
	.uleb128 0xa
	.4byte	.LASF18
	.byte	0x4
	.2byte	0x1da
	.4byte	0xca
	.byte	0x4
	.uleb128 0x9
	.ascii	"PAR\000"
	.byte	0x4
	.2byte	0x1db
	.4byte	0xca
	.byte	0x8
	.uleb128 0xa
	.4byte	.LASF19
	.byte	0x4
	.2byte	0x1dc
	.4byte	0xca
	.byte	0xc
	.uleb128 0xa
	.4byte	.LASF20
	.byte	0x4
	.2byte	0x1dd
	.4byte	0xca
	.byte	0x10
	.uleb128 0x9
	.ascii	"FCR\000"
	.byte	0x4
	.2byte	0x1de
	.4byte	0xca
	.byte	0x14
	.byte	0
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.2byte	0x1df
	.4byte	0xe9
	.uleb128 0x8
	.byte	0x10
	.byte	0x4
	.2byte	0x1e1
	.4byte	0x18a
	.uleb128 0xa
	.4byte	.LASF22
	.byte	0x4
	.2byte	0x1e3
	.4byte	0xca
	.byte	0
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x4
	.2byte	0x1e4
	.4byte	0xca
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x4
	.2byte	0x1e5
	.4byte	0xca
	.byte	0x8
	.uleb128 0xa
	.4byte	.LASF25
	.byte	0x4
	.2byte	0x1e6
	.4byte	0xca
	.byte	0xc
	.byte	0
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x4
	.2byte	0x1e7
	.4byte	0x14c
	.uleb128 0x8
	.byte	0x28
	.byte	0x4
	.2byte	0x28e
	.4byte	0x222
	.uleb128 0xa
	.4byte	.LASF27
	.byte	0x4
	.2byte	0x290
	.4byte	0xca
	.byte	0
	.uleb128 0xa
	.4byte	.LASF28
	.byte	0x4
	.2byte	0x291
	.4byte	0xca
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x292
	.4byte	0xca
	.byte	0x8
	.uleb128 0xa
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x293
	.4byte	0xca
	.byte	0xc
	.uleb128 0x9
	.ascii	"IDR\000"
	.byte	0x4
	.2byte	0x294
	.4byte	0xca
	.byte	0x10
	.uleb128 0x9
	.ascii	"ODR\000"
	.byte	0x4
	.2byte	0x295
	.4byte	0xca
	.byte	0x14
	.uleb128 0xa
	.4byte	.LASF31
	.byte	0x4
	.2byte	0x296
	.4byte	0xdf
	.byte	0x18
	.uleb128 0xa
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x297
	.4byte	0xdf
	.byte	0x1a
	.uleb128 0xa
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x298
	.4byte	0xca
	.byte	0x1c
	.uleb128 0x9
	.ascii	"AFR\000"
	.byte	0x4
	.2byte	0x299
	.4byte	0x222
	.byte	0x20
	.byte	0
	.uleb128 0x5
	.4byte	0xcf
	.uleb128 0xb
	.4byte	.LASF34
	.byte	0x4
	.2byte	0x29a
	.4byte	0x196
	.uleb128 0x8
	.byte	0x88
	.byte	0x4
	.2byte	0x2dd
	.4byte	0x3c2
	.uleb128 0x9
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x2df
	.4byte	0xca
	.byte	0
	.uleb128 0xa
	.4byte	.LASF35
	.byte	0x4
	.2byte	0x2e0
	.4byte	0xca
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x2e1
	.4byte	0xca
	.byte	0x8
	.uleb128 0x9
	.ascii	"CIR\000"
	.byte	0x4
	.2byte	0x2e2
	.4byte	0xca
	.byte	0xc
	.uleb128 0xa
	.4byte	.LASF37
	.byte	0x4
	.2byte	0x2e3
	.4byte	0xca
	.byte	0x10
	.uleb128 0xa
	.4byte	.LASF38
	.byte	0x4
	.2byte	0x2e4
	.4byte	0xca
	.byte	0x14
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0x4
	.2byte	0x2e5
	.4byte	0xca
	.byte	0x18
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x2e6
	.4byte	0xb8
	.byte	0x1c
	.uleb128 0xa
	.4byte	.LASF41
	.byte	0x4
	.2byte	0x2e7
	.4byte	0xca
	.byte	0x20
	.uleb128 0xa
	.4byte	.LASF42
	.byte	0x4
	.2byte	0x2e8
	.4byte	0xca
	.byte	0x24
	.uleb128 0xa
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x2e9
	.4byte	0xcf
	.byte	0x28
	.uleb128 0xa
	.4byte	.LASF44
	.byte	0x4
	.2byte	0x2ea
	.4byte	0xca
	.byte	0x30
	.uleb128 0xa
	.4byte	.LASF45
	.byte	0x4
	.2byte	0x2eb
	.4byte	0xca
	.byte	0x34
	.uleb128 0xa
	.4byte	.LASF46
	.byte	0x4
	.2byte	0x2ec
	.4byte	0xca
	.byte	0x38
	.uleb128 0xa
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x2ed
	.4byte	0xb8
	.byte	0x3c
	.uleb128 0xa
	.4byte	.LASF48
	.byte	0x4
	.2byte	0x2ee
	.4byte	0xca
	.byte	0x40
	.uleb128 0xa
	.4byte	.LASF49
	.byte	0x4
	.2byte	0x2ef
	.4byte	0xca
	.byte	0x44
	.uleb128 0xa
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x2f0
	.4byte	0xcf
	.byte	0x48
	.uleb128 0xa
	.4byte	.LASF51
	.byte	0x4
	.2byte	0x2f1
	.4byte	0xca
	.byte	0x50
	.uleb128 0xa
	.4byte	.LASF52
	.byte	0x4
	.2byte	0x2f2
	.4byte	0xca
	.byte	0x54
	.uleb128 0xa
	.4byte	.LASF53
	.byte	0x4
	.2byte	0x2f3
	.4byte	0xca
	.byte	0x58
	.uleb128 0xa
	.4byte	.LASF54
	.byte	0x4
	.2byte	0x2f4
	.4byte	0xb8
	.byte	0x5c
	.uleb128 0xa
	.4byte	.LASF55
	.byte	0x4
	.2byte	0x2f5
	.4byte	0xca
	.byte	0x60
	.uleb128 0xa
	.4byte	.LASF56
	.byte	0x4
	.2byte	0x2f6
	.4byte	0xca
	.byte	0x64
	.uleb128 0xa
	.4byte	.LASF57
	.byte	0x4
	.2byte	0x2f7
	.4byte	0xcf
	.byte	0x68
	.uleb128 0xa
	.4byte	.LASF58
	.byte	0x4
	.2byte	0x2f8
	.4byte	0xca
	.byte	0x70
	.uleb128 0x9
	.ascii	"CSR\000"
	.byte	0x4
	.2byte	0x2f9
	.4byte	0xca
	.byte	0x74
	.uleb128 0xa
	.4byte	.LASF59
	.byte	0x4
	.2byte	0x2fa
	.4byte	0xcf
	.byte	0x78
	.uleb128 0xa
	.4byte	.LASF60
	.byte	0x4
	.2byte	0x2fb
	.4byte	0xca
	.byte	0x80
	.uleb128 0xa
	.4byte	.LASF61
	.byte	0x4
	.2byte	0x2fc
	.4byte	0xca
	.byte	0x84
	.byte	0
	.uleb128 0xb
	.4byte	.LASF62
	.byte	0x4
	.2byte	0x2fd
	.4byte	0x233
	.uleb128 0x8
	.byte	0x24
	.byte	0x4
	.2byte	0x34f
	.4byte	0x4c0
	.uleb128 0x9
	.ascii	"CR1\000"
	.byte	0x4
	.2byte	0x351
	.4byte	0xdf
	.byte	0
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x352
	.4byte	0xa2
	.byte	0x2
	.uleb128 0x9
	.ascii	"CR2\000"
	.byte	0x4
	.2byte	0x353
	.4byte	0xdf
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x354
	.4byte	0xa2
	.byte	0x6
	.uleb128 0x9
	.ascii	"SR\000"
	.byte	0x4
	.2byte	0x355
	.4byte	0xdf
	.byte	0x8
	.uleb128 0xa
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x356
	.4byte	0xa2
	.byte	0xa
	.uleb128 0x9
	.ascii	"DR\000"
	.byte	0x4
	.2byte	0x357
	.4byte	0xdf
	.byte	0xc
	.uleb128 0xa
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x358
	.4byte	0xa2
	.byte	0xe
	.uleb128 0xa
	.4byte	.LASF63
	.byte	0x4
	.2byte	0x359
	.4byte	0xdf
	.byte	0x10
	.uleb128 0xa
	.4byte	.LASF54
	.byte	0x4
	.2byte	0x35a
	.4byte	0xa2
	.byte	0x12
	.uleb128 0xa
	.4byte	.LASF64
	.byte	0x4
	.2byte	0x35b
	.4byte	0xdf
	.byte	0x14
	.uleb128 0xa
	.4byte	.LASF57
	.byte	0x4
	.2byte	0x35c
	.4byte	0xa2
	.byte	0x16
	.uleb128 0xa
	.4byte	.LASF65
	.byte	0x4
	.2byte	0x35d
	.4byte	0xdf
	.byte	0x18
	.uleb128 0xa
	.4byte	.LASF59
	.byte	0x4
	.2byte	0x35e
	.4byte	0xa2
	.byte	0x1a
	.uleb128 0xa
	.4byte	.LASF66
	.byte	0x4
	.2byte	0x35f
	.4byte	0xdf
	.byte	0x1c
	.uleb128 0xa
	.4byte	.LASF67
	.byte	0x4
	.2byte	0x360
	.4byte	0xa2
	.byte	0x1e
	.uleb128 0xa
	.4byte	.LASF68
	.byte	0x4
	.2byte	0x361
	.4byte	0xdf
	.byte	0x20
	.uleb128 0xa
	.4byte	.LASF69
	.byte	0x4
	.2byte	0x362
	.4byte	0xa2
	.byte	0x22
	.byte	0
	.uleb128 0xb
	.4byte	.LASF70
	.byte	0x4
	.2byte	0x363
	.4byte	0x3ce
	.uleb128 0xc
	.4byte	.LASF81
	.byte	0x1
	.byte	0x3
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x516
	.uleb128 0xd
	.4byte	.LASF82
	.byte	0x1
	.byte	0x3
	.4byte	0xb8
	.4byte	.LLST0
	.uleb128 0xe
	.4byte	.LASF73
	.byte	0x1
	.byte	0x5
	.4byte	0xb8
	.4byte	.LLST1
	.uleb128 0xf
	.4byte	.LASF71
	.byte	0x1
	.byte	0x5
	.4byte	0xb8
	.uleb128 0xf
	.4byte	.LASF72
	.byte	0x1
	.byte	0x5
	.4byte	0xb8
	.byte	0
	.uleb128 0x10
	.4byte	.LASF83
	.byte	0x1
	.byte	0x41
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x53b
	.uleb128 0x11
	.4byte	.LASF74
	.byte	0x1
	.byte	0x42
	.4byte	0xb8
	.4byte	0xe1000
	.byte	0
	.uleb128 0x12
	.4byte	.LASF75
	.byte	0x5
	.2byte	0x51b
	.4byte	0xe4
	.uleb128 0x6
	.4byte	0xb8
	.4byte	0x558
	.uleb128 0x13
	.4byte	0xc3
	.2byte	0x1ff
	.byte	0
	.uleb128 0x14
	.4byte	.LASF76
	.byte	0x6
	.byte	0x8
	.4byte	0x569
	.uleb128 0x5
	.byte	0x3
	.4byte	buffer
	.uleb128 0x5
	.4byte	0x547
	.uleb128 0x6
	.4byte	0xa2
	.4byte	0x57f
	.uleb128 0x13
	.4byte	0xc3
	.2byte	0x1ff
	.byte	0
	.uleb128 0x14
	.4byte	.LASF77
	.byte	0x6
	.byte	0x9
	.4byte	0x56e
	.uleb128 0x5
	.byte	0x3
	.4byte	utmp16
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1c
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST0:
	.4byte	.LVL0-.Ltext0
	.4byte	.LVL1-.Ltext0
	.2byte	0x1
	.byte	0x50
	.4byte	.LVL1-.Ltext0
	.4byte	.LFE110-.Ltext0
	.2byte	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x50
	.byte	0x9f
	.4byte	0
	.4byte	0
.LLST1:
	.4byte	.LVL2-.Ltext0
	.4byte	.LVL3-.Ltext0
	.2byte	0x1
	.byte	0x53
	.4byte	.LVL3-.Ltext0
	.4byte	.LVL4-.Ltext0
	.2byte	0x5
	.byte	0x70
	.sleb128 0
	.byte	0x33
	.byte	0x25
	.byte	0x9f
	.4byte	.LVL4-.Ltext0
	.4byte	.LVL5-.Ltext0
	.2byte	0x28
	.byte	0xc
	.4byte	0x33428f00
	.byte	0xf7
	.uleb128 0x25
	.byte	0xf3
	.uleb128 0x1
	.byte	0x50
	.byte	0x38
	.byte	0x24
	.byte	0xf7
	.uleb128 0x25
	.byte	0x1b
	.byte	0xf7
	.uleb128 0
	.byte	0x23
	.uleb128 0x5
	.byte	0xf7
	.uleb128 0x25
	.byte	0xf7
	.uleb128 0x2c
	.byte	0x76
	.sleb128 0
	.byte	0xf7
	.uleb128 0x25
	.byte	0xf7
	.uleb128 0x2c
	.byte	0x1e
	.byte	0x8
	.byte	0x20
	.byte	0xf7
	.uleb128 0x2c
	.byte	0x25
	.byte	0xf7
	.uleb128 0x25
	.byte	0x33
	.byte	0x25
	.byte	0x9f
	.4byte	.LVL6-.Ltext0
	.4byte	.LVL7-.Ltext0
	.2byte	0x1
	.byte	0x53
	.4byte	.LVL7-.Ltext0
	.4byte	.LVL8-.Ltext0
	.2byte	0x5
	.byte	0x73
	.sleb128 0
	.byte	0x31
	.byte	0x25
	.byte	0x9f
	.4byte	.LVL8-.Ltext0
	.4byte	.LVL9-.Ltext0
	.2byte	0x1
	.byte	0x53
	.4byte	.LVL9-.Ltext0
	.4byte	.LFE110-.Ltext0
	.2byte	0x9
	.byte	0x70
	.sleb128 0
	.byte	0x33
	.byte	0x25
	.byte	0x31
	.byte	0x1c
	.byte	0x31
	.byte	0x25
	.byte	0x9f
	.4byte	0
	.4byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF51:
	.ascii	"AHB1LPENR\000"
.LASF22:
	.ascii	"LISR\000"
.LASF24:
	.ascii	"LIFCR\000"
.LASF41:
	.ascii	"APB1RSTR\000"
.LASF21:
	.ascii	"DMA_Stream_TypeDef\000"
.LASF4:
	.ascii	"short int\000"
.LASF17:
	.ascii	"sizetype\000"
.LASF58:
	.ascii	"BDCR\000"
.LASF61:
	.ascii	"PLLI2SCFGR\000"
.LASF10:
	.ascii	"__uint32_t\000"
.LASF83:
	.ascii	"initDMA1\000"
.LASF6:
	.ascii	"__uint16_t\000"
.LASF60:
	.ascii	"SSCGR\000"
.LASF65:
	.ascii	"TXCRCR\000"
.LASF53:
	.ascii	"AHB3LPENR\000"
.LASF64:
	.ascii	"RXCRCR\000"
.LASF47:
	.ascii	"RESERVED2\000"
.LASF50:
	.ascii	"RESERVED3\000"
.LASF13:
	.ascii	"uint8_t\000"
.LASF26:
	.ascii	"DMA_TypeDef\000"
.LASF48:
	.ascii	"APB1ENR\000"
.LASF69:
	.ascii	"RESERVED8\000"
.LASF77:
	.ascii	"utmp16\000"
.LASF28:
	.ascii	"OTYPER\000"
.LASF49:
	.ascii	"APB2ENR\000"
.LASF82:
	.ascii	"sample_rate\000"
.LASF46:
	.ascii	"AHB3ENR\000"
.LASF25:
	.ascii	"HIFCR\000"
.LASF12:
	.ascii	"long long int\000"
.LASF30:
	.ascii	"PUPDR\000"
.LASF23:
	.ascii	"HISR\000"
.LASF72:
	.ascii	"i2sdiv\000"
.LASF62:
	.ascii	"RCC_TypeDef\000"
.LASF32:
	.ascii	"BSRRH\000"
.LASF27:
	.ascii	"MODER\000"
.LASF42:
	.ascii	"APB2RSTR\000"
.LASF5:
	.ascii	"__uint8_t\000"
.LASF74:
	.ascii	"baudrate\000"
.LASF81:
	.ascii	"initMIC\000"
.LASF31:
	.ascii	"BSRRL\000"
.LASF79:
	.ascii	"mp45dt02.c\000"
.LASF3:
	.ascii	"unsigned char\000"
.LASF78:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O2 -fsingle-precision-constant\000"
.LASF2:
	.ascii	"signed char\000"
.LASF1:
	.ascii	"long long unsigned int\000"
.LASF16:
	.ascii	"uint32_t\000"
.LASF66:
	.ascii	"I2SCFGR\000"
.LASF0:
	.ascii	"unsigned int\000"
.LASF38:
	.ascii	"AHB2RSTR\000"
.LASF14:
	.ascii	"uint16_t\000"
.LASF11:
	.ascii	"long unsigned int\000"
.LASF36:
	.ascii	"CFGR\000"
.LASF35:
	.ascii	"PLLCFGR\000"
.LASF20:
	.ascii	"M1AR\000"
.LASF29:
	.ascii	"OSPEEDR\000"
.LASF55:
	.ascii	"APB1LPENR\000"
.LASF9:
	.ascii	"long int\000"
.LASF15:
	.ascii	"int32_t\000"
.LASF70:
	.ascii	"SPI_TypeDef\000"
.LASF68:
	.ascii	"I2SPR\000"
.LASF71:
	.ascii	"i2sodd\000"
.LASF52:
	.ascii	"AHB2LPENR\000"
.LASF63:
	.ascii	"CRCPR\000"
.LASF76:
	.ascii	"buffer\000"
.LASF40:
	.ascii	"RESERVED0\000"
.LASF43:
	.ascii	"RESERVED1\000"
.LASF19:
	.ascii	"M0AR\000"
.LASF54:
	.ascii	"RESERVED4\000"
.LASF57:
	.ascii	"RESERVED5\000"
.LASF59:
	.ascii	"RESERVED6\000"
.LASF67:
	.ascii	"RESERVED7\000"
.LASF18:
	.ascii	"NDTR\000"
.LASF80:
	.ascii	"/home/emir/msut/STM32F407/examples/dmai2sbkpFINAL\000"
.LASF7:
	.ascii	"short unsigned int\000"
.LASF44:
	.ascii	"AHB1ENR\000"
.LASF73:
	.ascii	"utmp32\000"
.LASF45:
	.ascii	"AHB2ENR\000"
.LASF8:
	.ascii	"__int32_t\000"
.LASF37:
	.ascii	"AHB1RSTR\000"
.LASF34:
	.ascii	"GPIO_TypeDef\000"
.LASF39:
	.ascii	"AHB3RSTR\000"
.LASF75:
	.ascii	"ITM_RxBuffer\000"
.LASF33:
	.ascii	"LCKR\000"
.LASF56:
	.ascii	"APB2LPENR\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
