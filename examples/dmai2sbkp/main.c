#include "stm32f4xx.h"
#include "usart.h"
#include "delay.h"
#include "mp45dt02.h"
#include "pdm_filter.h"

#define MIC_BUFFER_SIZE				512

PDMFilter_InitStruct Filter;

int main(void)
{
	uint32_t utmp32 = 0, k = 0, n;
	uint16_t buff[MIC_BUFFER_SIZE], cnt = 0; 
	uint16_t volume = 30;
	int16_t outdata[MIC_BUFFER_SIZE];
	uint16_t fs = 16000;
	
	Filter.LP_HZ = 8000;
	Filter.HP_HZ = 10;
	Filter.Fs = fs;
	Filter.Out_MicChannels = 1;
	Filter.In_MicChannels = 1;
	
	for(k=0;k<(MIC_BUFFER_SIZE);k++)
	{
		outdata[k] = 0x0000;
	}

	uint16_t arrcnt = 0;
	uint8_t arrflag = 0;

	initDMA1();
	initUSART2(USART2_BAUDRATE_460800);
	initMIC(2*fs);
	
	RCC->AHB1ENR |= RCC_AHB1ENR_CRCEN;
	PDM_Filter_Init((PDMFilter_InitStruct *)&Filter);  
															// init MEMS MIC with sample rate 8kHz 
	SPI2->I2SCFGR |= SPI_I2SCFGR_I2SE;   
                    


	while(1)
	{

		
		/* DMA1_Stream3->CR &= ~DMA_SxCR_EN; */
		DMA1->LIFCR |= DMA_LIFCR_CTCIF3;
		DMA1_Stream3->CR |= DMA_SxCR_EN;

		while(!(SPI2->SR & SPI_SR_RXNE));

		/* buff[cnt] = HTONS(utmp16[cnt]); */

/* 		uint16_t tmp16; */
/* 		if ((DMA1_Stream3->CR & DMA_SxCR_CT) == DMA_SxCR_CT) { */
/* 			tmp16 = utmp16[arrcnt]; */
/* 		} else { */
/* 			tmp16 = utmp16_2[arrcnt]; */
/* 		} */


/* 		arrcnt += arrflag; */
/* 		arrflag = arrflag ? 0 : 1; */


		/* buff[cnt] = HTONS(tmp16); */
		uint16_t *ptr = utmp16;
		for (; ptr < utmp16 + MIC_BUFFER_SIZE; ++ptr) {
			*ptr = HTONS(*ptr);
		}

		/* cnt++; */

		/* if (cnt >= (MIC_BUFFER_SIZE)) */
		/* { */

			/* uint8_t c = getcharUSART2(); */
			cnt = 512;
			volume = 30;
			
			k = 0;
			n = 0;
			while(k < cnt)
			{
				PDM_Filter_64_LSB((uint8_t *)&utmp16[k], (uint16_t *)&outdata[n], volume , (PDMFilter_InitStruct *)&Filter);
				/* PDM_Filter_64_LSB((uint8_t *)&buff[k], (uint16_t *)&outdata[n], volume , (PDMFilter_InitStruct *)&Filter); */
				k += 64;
				n += 16;

			}


			DMA2_Stream7->CR &= ~DMA_SxCR_EN;
			n = ((MIC_BUFFER_SIZE)/4);
			for(k=0;k<n;++k)
			{

				uint16_t utmp16_2 = outdata[k];
				/* txBuffer1[k] = ((utmp16 & 0xFF00)>> (8 * (k%2))); */
				if (k < n/2) {
					txBuffer1[k] = utmp16_2 >> 8;
					txBuffer1[++k] = utmp16_2;
				} else {
					txBuffer2[k - (n/2)] = utmp16_2 >> 8;
					txBuffer2[++k - (n/2)] = utmp16_2;
				}

			
			}


			DMA2_Stream7->CR |= DMA_SxCR_EN;
				while (!(USART1->SR & USART_SR_TXE));
				while (!(DMA2->HISR & DMA_HISR_TCIF7));

				DMA2->HIFCR |= DMA_HIFCR_CTCIF7;


			cnt = 0;

		/* } */	


	}
}
