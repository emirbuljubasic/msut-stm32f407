#ifndef __MP45DT02_H
#define __MP45DT02_H

#include "stm32f4xx.h"
#include "delay.h"

#define uart_buff_size 512
volatile uint32_t buffer[uart_buff_size];
uint16_t utmp16[uart_buff_size]; 
uint16_t utmp16_2[uart_buff_size];
/* uint16_t utmp16; */

void initDMA1();
void initMIC(uint32_t sample_rate);	

#endif 
