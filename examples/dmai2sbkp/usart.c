#include "usart.h"

void initUSART2(uint32_t baudrate)
{
	//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	// USART2: PA9 -> TX & PA10 -> RX
	//------------------------------------------------------------------ 
	
	initDMA2();

	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN; 									
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN; 									
	GPIOA->MODER |= (GPIO_MODER_MODER9_1)|(GPIO_MODER_MODER10_1); 		
	GPIOA->AFR[1] |= 0x00000770;										
	
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9_1;							  
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR10_1;							  
	
	USART1->BRR = baudrate;

	USART1->CR1 = 0;
	
	USART1->CR3 |= (USART_CR3_DMAT);

	USART1->CR1 = (USART_CR1_UE)|(USART_CR1_TE)|(USART_CR1_RE);						
}

void putcharUSART2(uint8_t data)
{/// print one character to USART2
	/* while(!(USART1->SR & USART_SR_TC)); */									
	/* while(!(DMA2->HISR & DMA_HISR_TCIF7)); */

	/* DMA2_Stream7->CR |= DMA_SxCR_EN; */

	/* txBuffer[0] = data; */

	/* while(!(DMA2->HISR & DMA_HISR_TCIF7) && (DMA2_Stream7->NDTR != 0)); */

	/* DMA2_Stream7->CR &= ~DMA_SxCR_EN; */
	/* while (DMA2_Stream7->CR & DMA_SxCR_EN); */
	/* DMA2->HIFCR |= DMA_HIFCR_CTCIF7; */

	/* USART2->DR = data; */												    
}

void initDMA2() {

	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;

	/* DMA2_Stream5->CR &= ~DMA_SxCR_EN; // RX */
	DMA2_Stream7->CR &= ~DMA_SxCR_EN; // TX
	
	/* while (DMA2_Stream5->CR & DMA2_Stream7->CR & DMA_SxCR_EN); */

	/* DMA2_Stream5->CR &= 0xF01000000; */
	DMA2_Stream7->CR &= 0xF01000000;

	DMA2->LISR ^= DMA2->LISR;
	DMA2->HISR ^= DMA2->HISR;

	//TX

	DMA2_Stream7->PAR = (uint32_t) &USART1->DR;
	DMA2_Stream7->M0AR = (uint32_t) txBuffer1;
	DMA2_Stream7->M1AR = (uint32_t) txBuffer2;



	DMA2_Stream7->NDTR = TX_BUFFER_SIZE;
	DMA2_Stream7->CR |= DMA_SxCR_CHSEL_2;
	DMA2_Stream7->CR |= DMA_SxCR_DBM;

	DMA2_Stream7->CR |= DMA_SxCR_PL;
	DMA2_Stream7->CR |= DMA_SxCR_DIR_0;

	DMA2_Stream7->CR |= DMA_SxCR_MINC;
	DMA2_Stream7->CR |= DMA_SxCR_TCIE;

	/* DMA2_Stream7->CR |= DMA_SxCR_MSIZE_0; */
	/* DMA2_Stream7->CR |= DMA_SxCR_PSIZE_0; */

	DMA2_Stream7->CR &= ~DMA_SxCR_MSIZE;
	DMA2_Stream7->CR &= ~DMA_SxCR_PSIZE;

	DMA2_Stream7->CR |= DMA_SxCR_EN;

	//RX
	/* DMA2_Stream5->PAR = (uint32_t) &USART1->DR; */
	/* DMA2_Stream5->M0AR = (uint32_t) rxBuffer; */

	/* DMA2_Stream5->NDTR = RX_BUFFER_SIZE; */
	/* DMA2_Stream5->CR |= DMA_SxCR_CHSEL_2; */

	/* DMA2_Stream5->CR |= DMA_SxCR_PL_1; */
	/* DMA2_Stream5->CR &= ~DMA_SxCR_DIR; */

	/* DMA2_Stream5->CR |= DMA_SxCR_MINC; */
	/* DMA2_Stream5->CR |= DMA_SxCR_TCIE; */

	/* DMA2_Stream5->CR |= DMA_SxCR_MSIZE_1; */
	/* DMA2_Stream5->CR |= DMA_SxCR_PSIZE_1; */


	/* DMA2_Stream5->CR |= DMA_SxCR_EN; */


}

void printUSART2(char *str, ... )
{ /// print text and up to 10 arguments!
    uint8_t rstr[40];													// 33 max -> 32 ASCII for 32 bits and NULL 
    uint16_t k = 0;
	uint16_t arg_type;
	uint32_t utmp32;
	uint32_t * p_uint32; 
	char * p_char;
	va_list vl;
	
	//va_start(vl, 10);													// always pass the last named parameter to va_start, for compatibility with older compilers
	va_start(vl, str);													// always pass the last named parameter to va_start, for compatibility with older compilers
	while(str[k] != 0x00)
	{
		if(str[k] == '%')
		{
			if(str[k+1] != 0x00)
			{
				switch(str[k+1])
				{
					case('b'):
					{// binary
						if(str[k+2] == 'b')
						{// byte
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_BINARY_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_BINARY_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
						}
						else
						{// default word
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
							k--;
						}
						
						k++;	
						p_uint32 = &utmp32;
						break;
					}
					case('d'):
					{// decimal
						if(str[k+2] == 'b')
						{// byte
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_WORD);
						}
						else
						{// default word
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_WORD);
							k--;
						}
						
						k++;	
						p_uint32 = &utmp32;
						break;
					}
					case('c'):
					{// character
						char tchar = va_arg(vl, int);	
						putcharUSART2(tchar);
						arg_type = (PRINT_ARG_TYPE_CHARACTER);
						break;
					}
					case('s'):
					{// string 
						p_char = va_arg(vl, char *);	
						sprintUSART2((uint8_t *)p_char);
						arg_type = (PRINT_ARG_TYPE_STRING);
						break;
					}
					case('f'):
					{// float
						uint64_t utmp64 = va_arg(vl, uint64_t);			// convert double to float representation IEEE 754
						uint32_t tmp1 = utmp64&0x00000000FFFFFFFF;
						tmp1 = tmp1>>29;
						utmp32 = utmp64>>32;
						utmp32 &= 0x07FFFFFF;
						utmp32 = utmp32<<3;
						utmp32 |= tmp1;
						if(utmp64 & 0x8000000000000000)
							utmp32 |= 0x80000000;
							
						if(utmp64 & 0x4000000000000000)
							utmp32 |= 0x40000000;
							
						p_uint32 = &utmp32;
						
						arg_type = (PRINT_ARG_TYPE_FLOAT);
						//arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
						//arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
						break;
					}
					case('x'):
					{// hexadecimal 
						if(str[k+2] == 'b')
						{// byte
							utmp32 = (uint32_t)va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = (uint32_t)va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
						}
						else
						{
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
							k--;
						}
						
						k++;
						p_uint32 = &utmp32;
						break;
					}
					default:
					{
						utmp32 = 0;
						p_uint32 = &utmp32;
						arg_type = (PRINT_ARG_TYPE_UNKNOWN);
						break;
					}
				}
					
				if(arg_type&(PRINT_ARG_TYPE_MASK_CHAR_STRING))	
				{
					getStr4NumMISC(arg_type, p_uint32, rstr);
					sprintUSART2(rstr);	
				}
				k++;
			}
		}
		else
		{// not a '%' char -> print the char
			putcharUSART2(str[k]);
			if (str[k] == '\n')
				putcharUSART2('\r');
		}
		k++;
	}
	
	va_end(vl);
	return;
}

void sprintUSART2(uint8_t * str)
{
	uint16_t k = 0;
	
	while (str[k] != '\0')
    {
        putcharUSART2(str[k]);
        if (str[k] == '\n')
            putcharUSART2('\r');
        k++;

        if (k == MAX_PRINT_STRING_SIZE)
            break;
    }
}

uint8_t getcharUSART2(void)
{/// get one character from USART1
	uint8_t data;
	/* USART2->CR1 |= USART_CR1_UE|USART_CR1_RE;							// enable usart	and Rx */
	/* while((USART2->SR & USART_SR_RXNE) != USART_SR_RXNE);				// wait until data ready */
	
	data = USART2->DR;													// send data
	/* USART2->SR &= ~(USART_SR_RXNE);										// clear Rx data ready flag */
	/* USART2->CR1 &= ~(USART_CR1_RE); */
	return data;
}
