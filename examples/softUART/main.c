#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "usart.h"

int main(void) {

	initUSART2(USART2_BAUDRATE_9600);

	uint8_t num = 9;
	uint8_t c = 33;


	while(1){
		printUSART2("Hello, World!\n");
		printUSART2("USART2PRINT\n");
	}

	return 0;
}
