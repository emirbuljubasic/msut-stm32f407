#include "usart.h"


void initGPIOE();
void initTIM1(uint32_t baudrate);

void initUSART2(uint32_t baudrate)
{
	//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	// USART2: PE2 -> TX & PXX -> RX
	//------------------------------------------------------------------ 
	
	initGPIOE();
	initTIM1(baudrate); 
	initDMA2();
}

void initGPIOE() {

	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN;
	// 0000 0000 0000 0000 0000 0000 0100 0000
	GPIOE->MODER |= 0x00000040; // SET PE3 to GPO
	GPIOE->OTYPER = 0x00000000;
	GPIOE->OSPEEDR |= 0x000000C0;
	GPIOE->PUPDR &= ~GPIO_PUPDR_PUPDR3;
}

void initTIM1(uint32_t baudrate) {
	//TIM INIT
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;

	TIM1->CR1 &= ~TIM_CR1_CEN;

	while (TIM1->CR1 & TIM_CR1_CEN);

    // Configure TIM1 for the desired baud rate
	TIM1->PSC = 0;
	TIM1->ARR = SystemCoreClock / baudrate - 1;

	TIM1->DIER |= TIM_DIER_CC1DE;
	TIM1->EGR |= TIM_EGR_CC1G;
}

void initDMA2() {
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;

    DMA2_Stream1->CR &= ~DMA_SxCR_EN;  // Disable the DMA stream
	while (DMA2_Stream1->CR & DMA_SxCR_EN); // wait for DMA to disable

	DMA2_Stream1->CR &= 0xF01000000;

	DMA2->LISR ^= DMA2->LISR;
	DMA2->HISR ^= DMA2->HISR;

	/* DMA2_Stream1->CR |= DMA_SxCR_DBM; */
    DMA2_Stream1->PAR = (uint32_t) &(GPIOE->BSRR);  
	DMA2_Stream1->M0AR = (uint32_t) txBuffer; 
	/* DMA2_Stream1->M1AR = (uint32_t) txBuffer2; */

    DMA2_Stream1->NDTR = TX_BUFFER_SIZE;  // Number of data items to transfer
	
	DMA2_Stream1->CR |= DMA_SxCR_CHSEL_1 | DMA_SxCR_CHSEL_2; 

	DMA2_Stream1->CR |= DMA_SxCR_PL_1;

	DMA2_Stream1->CR |= DMA_SxCR_DIR_0;
	DMA2_Stream1->CR |= DMA_SxCR_MINC;
	DMA2_Stream1->CR |= DMA_SxCR_TCIE;

	DMA2_Stream1->CR |= DMA_SxCR_MSIZE_1;
	DMA2_Stream1->CR |= DMA_SxCR_PSIZE_1;

    DMA2_Stream1->CR |= DMA_SxCR_EN;  // Enable the DMA stream
}

void prepareBuffer(uint8_t data) {

	uint32_t dataTemp = ((uint32_t) data << 1) | 0x0200; 

    uint8_t i = 0;
	while (i < 10) {	
		txBuffer[i++] = ((dataTemp >> i) & 0x000000001) ? peHigh : peLow;	
	}
}

void putcharUSART2(uint8_t data) {
	prepareBuffer(data);

    DMA2_Stream1->CR |= DMA_SxCR_EN;  // Enable the DMA stream
	TIM1->CR1 |= TIM_CR1_CEN;

	while (!(DMA2->LISR & DMA_LISR_TCIF1) && (DMA2_Stream1->NDTR != 0));

	TIM1->CR1 &= ~TIM_CR1_CEN;
	
    DMA2_Stream1->CR &= ~DMA_SxCR_EN;  // Disable the DMA stream
	while (DMA2_Stream1->CR & DMA_SxCR_EN); // wait for DMA to disable
	DMA2->LIFCR = DMA_LIFCR_CTCIF1 | DMA_LIFCR_CHTIF1;
}

void printUSART2(char *str, ... )
{ /// print text and up to 10 arguments!
    uint8_t rstr[40];													// 33 max -> 32 ASCII for 32 bits and NULL 
    uint16_t k = 0;
	uint16_t arg_type;
	uint32_t utmp32;
	uint32_t * p_uint32; 
	char * p_char;
	va_list vl;
	
	//va_start(vl, 10);													// always pass the last named parameter to va_start, for compatibility with older compilers
	va_start(vl, str);													// always pass the last named parameter to va_start, for compatibility with older compilers
	while(str[k] != 0x00)
	{
		if(str[k] == '%')
		{
			if(str[k+1] != 0x00)
			{
				switch(str[k+1])
				{
					case('b'):
					{// binary
						if(str[k+2] == 'b')
						{// byte
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_BINARY_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_BINARY_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
						}
						else
						{// default word
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
							k--;
						}
						
						k++;	
						p_uint32 = &utmp32;
						break;
					}
					case('d'):
					{// decimal
						if(str[k+2] == 'b')
						{// byte
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_WORD);
						}
						else
						{// default word
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_DECIMAL_WORD);
							k--;
						}
						
						k++;	
						p_uint32 = &utmp32;
						break;
					}
					case('c'):
					{// character
						char tchar = va_arg(vl, int);	
						putcharUSART2(tchar);
						arg_type = (PRINT_ARG_TYPE_CHARACTER);
						break;
					}
					case('s'):
					{// string 
						p_char = va_arg(vl, char *);	
						sprintUSART2((uint8_t *)p_char);
						arg_type = (PRINT_ARG_TYPE_STRING);
						break;
					}
					case('f'):
					{// float
						uint64_t utmp64 = va_arg(vl, uint64_t);			// convert double to float representation IEEE 754
						uint32_t tmp1 = utmp64&0x00000000FFFFFFFF;
						tmp1 = tmp1>>29;
						utmp32 = utmp64>>32;
						utmp32 &= 0x07FFFFFF;
						utmp32 = utmp32<<3;
						utmp32 |= tmp1;
						if(utmp64 & 0x8000000000000000)
							utmp32 |= 0x80000000;
							
						if(utmp64 & 0x4000000000000000)
							utmp32 |= 0x40000000;
							
						p_uint32 = &utmp32;
						
						arg_type = (PRINT_ARG_TYPE_FLOAT);
						//arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
						//arg_type = (PRINT_ARG_TYPE_BINARY_WORD);
						break;
					}
					case('x'):
					{// hexadecimal 
						if(str[k+2] == 'b')
						{// byte
							utmp32 = (uint32_t)va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_BYTE);
						}
						else if(str[k+2] == 'h')
						{// half word
							utmp32 = (uint32_t)va_arg(vl, int);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_HALFWORD);
						}
						else if(str[k+2] == 'w')
						{// word	
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
						}
						else
						{
							utmp32 = va_arg(vl, uint32_t);
							arg_type = (PRINT_ARG_TYPE_HEXADECIMAL_WORD);
							k--;
						}
						
						k++;
						p_uint32 = &utmp32;
						break;
					}
					default:
					{
						utmp32 = 0;
						p_uint32 = &utmp32;
						arg_type = (PRINT_ARG_TYPE_UNKNOWN);
						break;
					}
				}
					
				if(arg_type&(PRINT_ARG_TYPE_MASK_CHAR_STRING))	
				{
					getStr4NumMISC(arg_type, p_uint32, rstr);
					sprintUSART2(rstr);	
				}
				k++;
			}
		}
		else
		{// not a '%' char -> print the char
			putcharUSART2(str[k]);
			if (str[k] == '\n')
				putcharUSART2('\r');
		}
		k++;
	}
	
	va_end(vl);
	return;
}

void sprintUSART2(uint8_t * str)
{
	uint16_t k = 0;
	
	while (str[k] != '\0')
    {
        putcharUSART2(str[k]);
        if (str[k] == '\n')
            putcharUSART2('\r');
        k++;

        if (k == MAX_PRINT_STRING_SIZE)
            break;
    }
}

uint8_t getcharUSART2(void)
{/// get one character from USART1
	uint8_t data;
	USART2->CR1 |= USART_CR1_UE|USART_CR1_RE;							// enable usart	and Rx
	while((USART2->SR & USART_SR_RXNE) != USART_SR_RXNE);				// wait until data ready
	
	data = USART2->DR;													// send data
	USART2->SR &= ~(USART_SR_RXNE);										// clear Rx data ready flag
	USART2->CR1 &= ~(USART_CR1_RE);
	return data;
}
