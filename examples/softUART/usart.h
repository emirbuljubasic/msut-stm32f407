#ifndef __USART2_H_
#define __USART2_H_

#include <stdio.h>
#include <stdarg.h>
#include "stm32f4xx.h"
#include "misc.h"

#define USART2_BAUDRATE_921600		921600	
#define USART2_BAUDRATE_460800		460800	
#define USART2_BAUDRATE_115200		115200	
#define USART2_BAUDRATE_9600		9600	

#define peHigh 0x00000008
#define peLow 0x00080000
#define TX_BUFFER_SIZE 10

volatile uint32_t txBuffer[TX_BUFFER_SIZE];
void initGPIOE();
void initTIM1(uint32_t baudrate);
void initDMA2();

void initUSART2(uint32_t baudrate);
void putcharUSART2(uint8_t data);
void printUSART2(char * str, ... );
void sprintUSART2(uint8_t * str);

#endif 
